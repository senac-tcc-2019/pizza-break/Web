import React from 'react'
import Sidebar from '../fixedComponents/sidebar';
import RelatoryList from './RelatoryList';

function Relatory() {
    return(
        <div>
        <Sidebar />
            <RelatoryList />
        </div>
    )
}

export default Relatory
